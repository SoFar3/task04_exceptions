package com.epam.utils;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static List<String> separate(String s, int partSize) {
        List<String> parts = new ArrayList<>();
        String temp = s;
        for (int i = 0; i < s.length(); i += partSize) {
            parts.add(temp.substring(0, Math.min(partSize, temp.length())));
            temp = temp.substring(Math.min(partSize, temp.length()));
        }
        return parts;
    }

}
