package com.epam.exception;

public class UserExistsException extends UserException {

    public UserExistsException(String message) {
        super(message);
    }

}
