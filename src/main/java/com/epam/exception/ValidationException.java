package com.epam.exception;

public class ValidationException extends Exception {

    public ValidationException(String message) {
        super(message);
    }

}
