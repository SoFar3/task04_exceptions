package com.epam.exception;

public class CloseResourceException extends RuntimeException {

    public CloseResourceException(String message) {
        super(message);
    }

}
