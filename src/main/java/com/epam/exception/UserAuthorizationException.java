package com.epam.exception;

public class UserAuthorizationException extends UserException {

    public UserAuthorizationException(String message) {
        super(message);
    }

}
