package com.epam.controller;

import com.epam.exception.ValidationException;
import com.epam.model.Resource;
import com.epam.model.Server;
import com.epam.model.User;

import java.util.List;

public class Controller {

    private Server server = new Server();

    public Controller() {
    }

    public void register(User user) throws ValidationException {
        server.register(user);
    }

    public void authorize(User user) {
        server.authorize(user);
    }

    public List<User> getAuthorizedUsers() {
        return server.getAuthorizedUsers();
    }

    public Resource getFileResource(User user, String fileName) {
        return server.getFileResource(user, fileName);
    }

}
