package com.epam.service;

import com.epam.exception.UserExistsException;
import com.epam.exception.ValidationException;
import com.epam.model.User;
import com.epam.model.Validator;

public class RegistrationService {

    private UserService userService;
    private Validator validator;

    private RegistrationService() {
        validator = new Validator();
    }

    public RegistrationService(UserService userService) {
        this();
        this.userService = userService;
    }

    public boolean register(User user) throws ValidationException {
        if (validator.validate(user)) {
            if (userService.getUserByUsername(user.getUsername()) != null) {
                return false;
            }
            userService.addUser(user);
        }
        return true;
    }

}
