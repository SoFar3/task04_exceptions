package com.epam.service;

import com.epam.model.User;

public class AuthorizationService {

    private UserService userService;

    public AuthorizationService(UserService userService) {
        this.userService = userService;
    }

    public boolean authenticate(User user) {
        User authenticatedUser = userService.getUserByUsername(user.getUsername());
        if (authenticatedUser == null) {
            return false;
        } else {
            return authenticatedUser.getPassword().equals(user.getPassword());
        }
    }

}
