package com.epam.service;

import com.epam.model.User;
import com.epam.model.UserRepo;

public class UserService {

    private UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public void addUser(User user) {
        userRepo.addUser(user);
    }

    public User getUserByUsername(final String username) {
        return userRepo.getUserByUsername(username);
    }

}
