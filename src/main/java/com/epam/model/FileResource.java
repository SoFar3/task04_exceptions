package com.epam.model;

import com.epam.exception.CloseResourceException;
import com.epam.exception.ResourceException;
import com.epam.utils.Utils;

import java.io.*;
import java.util.List;

public class FileResource implements Resource, AutoCloseable {

    private File file;

    public FileResource(String fileName) {
        file = new File("C:\\Users\\ytepl\\Dropbox\\EPAM\\online\\task04_exceptions\\" + fileName);
    }

    public void write(String str) throws ResourceException {
        write(str, false);
    }

    public void write(String str, boolean append) throws ResourceException {
        createFile();
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, append))) {
            List<String> blocks = Utils.separate(str, 8);
            for (String block : blocks) {
                bw.write(block);
            }
        } catch (IOException e) {
            throw new ResourceException(e.getMessage());
        }
    }

    public String read() throws ResourceException {
        createFile();
        StringBuilder result = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            while (br.ready()) {
                result.append(br.readLine());
            }
        } catch (IOException e) {
            throw new ResourceException(e.getMessage());
        }
        return result.toString();
    }

    @Override
    public void clear() throws ResourceException {
        write("");
    }

    private void createFile() throws ResourceException {
        if ( ! file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new ResourceException("Server can't create file with name: " + file.getName());
            }
        }
    }

    @Override
    public void close() {
        throw new CloseResourceException("Server failed while closing resource");
    }

}
