package com.epam.model;

import com.epam.exception.ResourceException;

public interface Resource extends AutoCloseable {

    void write(String str) throws ResourceException;
    void write(String str, boolean append) throws ResourceException;
    String read() throws ResourceException;
    void clear() throws ResourceException;

}
