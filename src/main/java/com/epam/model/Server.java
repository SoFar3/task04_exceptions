package com.epam.model;

import com.epam.exception.BadCredentialsException;
import com.epam.exception.UserAuthorizationException;
import com.epam.exception.UserExistsException;
import com.epam.exception.ValidationException;
import com.epam.service.AuthorizationService;
import com.epam.service.RegistrationService;
import com.epam.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class Server {

    private List<User> authorizedUsers;
    private RegistrationService registrationService;
    private AuthorizationService authorizationService;

    public Server() {
        authorizedUsers = new ArrayList<>();
        UserService userService = new UserService(new UserRepo());
        registrationService = new RegistrationService(userService);
        authorizationService = new AuthorizationService(userService);
    }

    public void register(User user) throws ValidationException {
        if ( ! registrationService.register(user)) {
            throw new UserExistsException("User with username: " + user.getUsername() + " already exists");
        }

    }

    public void authorize(User user) {
        if (authorizationService.authenticate(user)) {
            if (authorizedUsers.contains(user)) {
                throw new UserAuthorizationException("User with username: " + user.getUsername() + " is already authorized");
            }
            authorizedUsers.add(user);
        } else {
            throw new BadCredentialsException("Bad credentials");
        }
    }

    public List<User> getAuthorizedUsers() {
        return authorizedUsers;
    }

    public Resource getFileResource(User user, String fileName) {
        if ( ! isAuthorized(user.getUsername())) {
            throw new UserAuthorizationException("User with username: " + user.getUsername() + " isn't authorized");
        }
        return new FileResource(fileName);
    }

    private boolean isAuthorized(String username) {
        return authorizedUsers
                .stream()
                .anyMatch(
                        u -> u.getUsername().equals(username)
                );
    }

}
