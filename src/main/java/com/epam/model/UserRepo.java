package com.epam.model;

import com.epam.exception.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepo {

    private List<User> users;

    public UserRepo() {
        users = new ArrayList<>();
    }

    public void addUser(User user) {
        users.add(user);
    }

    public User getUserByUsername(final String username) {
        Optional<User> first = users.stream().filter(u -> u.getUsername().equals(username)).findFirst();
        return first.orElse(null);
    }

}
