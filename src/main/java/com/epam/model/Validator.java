package com.epam.model;

import com.epam.exception.ValidationException;

public class Validator {

    public boolean validate(User user) throws ValidationException {
        if (user.getUsername().length() < 4 || user.getUsername().length() > 32) {
            throw new ValidationException("Username size must be between 4 and 32 characters");
        }

        if (user.getPassword().length() < 8 || user.getPassword().length() > 64) {
            throw new ValidationException("Password size must be between 8 and 64 characters");
        }

        return true;
    }

}
