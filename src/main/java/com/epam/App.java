package com.epam;

import com.epam.exception.ResourceException;
import com.epam.exception.ValidationException;
import com.epam.view.ConsoleView;

public class App {

    public static void main(String[] args) throws ValidationException, ResourceException {
        (new ConsoleView()).show();
    }

}